# OpenWRT ddns模块支持阿里云的脚本

## 编译方法
### 准备代码
把aliyun.com.json、Makefile、update_aliyun_com.sh三个文件放到OpenWRT源码的package目录下，推荐使用package\3rdparty\ddns-scripts_aliyun

### 打开编译配置
make menuconfig
需要打开的编译项：luci-app-ddns， ddns-scripts_aliyun，选中后自动加入对应依赖（+ddns-scripts +curl +jshn +openssl-util）

### 编译
make -j3 V=s